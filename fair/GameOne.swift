//
//  GameOne.swift
//  fair
//
//  Created by Wong Kin Hon on 23/05/2017.
//  Copyright © 2017 Wong Kin Hon. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameOne: SKScene {
    
    // Tutorial
    var bIsTriggerTutorial = false
    var bIsTriggerSwipe = false
    var bIsTriggerExhausted = false
    var bIsTriggerSamePerson = false
    var triggerString = ""
    
    let blackBG = SKSpriteNode()
    
    let shape = SKShapeNode()
    let shape2 = SKShapeNode()
    
    let fingerShow = SKSpriteNode(imageNamed: "finger_up")
    let tutorial_label = SKLabelNode(fontNamed: "AllerDisplay")
    
    // start game timer
    var timeCount = 3
    
    // timer
    let mid_timer_label = SKLabelNode(fontNamed:"AllerDisplay")
    let timer_label = SKLabelNode(fontNamed:"AllerDisplay")
    var second = 0
    var timer = Timer()
    var isTimerRunning = false
    
    // draggable document timer
    var secondCalculatePercentage = 0
    var documentTimerPercentage:CGFloat = 1
    
    // Text
    let text_label = SKLabelNode(fontNamed:"H.H. Samuel")
    let text_label2 = SKLabelNode(fontNamed:"H.H. Samuel")
    
    // document
    let draggableDocument_BG = SKSpriteNode(imageNamed: "next_frame")
    let documentTimer = SKSpriteNode(imageNamed: "timer")
    let draggableDocument = SKSpriteNode(imageNamed: "document_0")
    let staticDraggableDocumentCount = SKLabelNode(fontNamed:"AllerDisplay")
    var currentDragNumber = -1
    var documentArr = [Int]()
    var currentDocumentArr = [Int]()
    var workerCurrentDocument = [Int]()
    var workerBoredAction = [Bool]()
    var workerBodyColorArr = [Int]()
    var workerFaceColorArr = [Int]()
    
    // set all sprite into array
    var workerBodySpriteArr = [SKSpriteNode]()
    var workerFaceSpriteArr = [SKSpriteNode]()
    var workerGlassesSpriteArr = [SKSpriteNode]()
    var workerExpressionIndicatorSpriteArr = [SKSpriteNode]()
    var workerExpressionSpriteArr = [SKSpriteNode]()
    var workerComputerSpriteArr = [SKSpriteNode]()
    var workerBGSpriteArr = [SKSpriteNode]()
    var workerDocumentSpriteArr = [SKSpriteNode]()
    var workerDeadSpriteArr = [SKSpriteNode]()
    var workerFillingBarSpriteArr = [SKSpriteNode]()
    var workerColorFillingBarSpriteArr = [SKSpriteNode]()
    
    // score
    var currentScore = 0
    var highscore = 0
    var bIsGameEnd = false
    var bIsGameStart = false
    
    // init varible
    var workerNameArr = [String]()
    var CboyName = [String]()
    var CgirlName = [String]()
    var MboyName = [String]()
    var MgirlName = [String]()
    var IboyName = [String]()
    var IgirlName = [String]()
    
    let maxWorkers = 6
    let maxDocument = 6
    
    let minJob = 3
    let maxJob = 6
    
    let documentFramePositionY:CGFloat = 1440
    let documentPositionY:CGFloat = 1415
    
    //    var textureAtlas_1 = SKTextureAtlas()
    var textureArray_1 = [SKTexture]()
    
    var textureAtlas_2 = SKTextureAtlas()
    var textureArray_2 = [SKTexture]()
    
    var textureAtlas_3 = SKTextureAtlas()
    var textureArray_3 = [SKTexture]()
    
    var previousAssign = -2
    
    var deadPeople = 0
    
    // sound
    let sound_dropDocument = SKAction.playSoundFileNamed("drop_document.mp3", waitForCompletion: false)
    let sound_fainted = SKAction.playSoundFileNamed("fainted.mp3", waitForCompletion: false)
    let sound_key01 = SKAction.playSoundFileNamed("key01.mp3", waitForCompletion: true)
    let sound_key02 = SKAction.playSoundFileNamed("key02.mp3", waitForCompletion: false)
    let sound_key03 = SKAction.playSoundFileNamed("key03.mp3", waitForCompletion: false)
    let sound_sad = SKAction.playSoundFileNamed("sad.mp3", waitForCompletion: false)
    
    // end game
    let endGameButton = SKSpriteNode(imageNamed: "btn_continue_01")
    
    var playAbleWidth: CGFloat = 0
    
    override func didMove(to view: SKView) {
        
        let screenSize = UIScreen.main.bounds
        let maxAspectRatio: CGFloat = screenSize.height / screenSize.width
        playAbleWidth = size.height / maxAspectRatio
        
        // start new game
        startNewGame()
    }
    
    func draw_Rect() {
        
        //        outer box : D3E5F5 - 211,229,245
        //        inner box : FFFFFF - 255,255,255
        
        let shapeSize: CGFloat = playAbleWidth - 50
        shape.path = UIBezierPath(roundedRect: CGRect(x: -(shapeSize / 2), y: -100, width: shapeSize, height: 220), cornerRadius: 30).cgPath
        shape.position = CGPoint(x: frame.midX, y: 1770)
        shape.fillColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.strokeColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.lineWidth = 10
        shape.zPosition = 11
        addChild(shape)
        
        let shape2Size: CGFloat = playAbleWidth - 90
        shape2.path = UIBezierPath(roundedRect: CGRect(x: -(shape2Size / 2), y: -80, width: shape2Size , height: 170), cornerRadius: 20).cgPath
        shape2.position = CGPoint(x: frame.midX, y: 1780)
        shape2.fillColor = UIColor.white
        shape2.strokeColor = UIColor.white
        shape2.lineWidth = 10
        shape2.zPosition = 12
        addChild(shape2)
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func updateTimer() {
        second += 1
        currentScore += 1
        secondCalculatePercentage += 1
        timer_label.text = timeString(time: TimeInterval(second))
        
        if secondCalculatePercentage == 10 {
            documentTimerPercentage -= 0.1
            secondCalculatePercentage = 0
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    func workerSetup() {
        // first worker.X starting point
        var firstX = frame.midX - 400
        // first worker.Y starting point
        var firstY = frame.midY - 235
        var percent = 1
        
        // Chinese male name
        CboyName.append("Wang")
        CboyName.append("Wei Han")
        CboyName.append("Jackson")
        CboyName.append("Andrew")
        CboyName.append("Michael")
        CboyName.append("Jordan")
        CboyName.append("Jack")
        CboyName.append("Harris")
        CboyName.append("Jason")
        CboyName.append("Chou")
        CboyName.append("Michael Kuehner")
        CboyName.append("Thomas Hundt")
        // Chinese female name
        CgirlName.append("Michelle")
        CgirlName.append("Ann")
        CgirlName.append("Kim")
        CgirlName.append("Zoe")
        CgirlName.append("Michelle")
        CgirlName.append("Amanda")
        CgirlName.append("Chloe")
        CgirlName.append("Sharon")
        CgirlName.append("Joanne")
        CgirlName.append("Nancy")
        // Indian male name
        IboyName.append("Aditya")
        IboyName.append("Ankit")
        IboyName.append("Deepak")
        IboyName.append("Vinay")
        IboyName.append("Pranav")
        IboyName.append("Krishna")
        IboyName.append("Shaurya")
        IboyName.append("Rohan")
        IboyName.append("Shivansh")
        IboyName.append("Abeer")
        IboyName.append("Supun Weerasinghe")
        IboyName.append("Suresh Sidhu")
        // Indian female name
        IgirlName.append("Ishita")
        IgirlName.append("Nikita")
        IgirlName.append("Aadhya")
        IgirlName.append("Kyra")
        IgirlName.append("Anaisha")
        IgirlName.append("Sarah")
        IgirlName.append("Shanaya")
        IgirlName.append("Meera")
        IgirlName.append("Mishka")
        IgirlName.append("Vedhika")
        IgirlName.append("Sarah")
        // Malay male name
        MboyName.append("Syafiq")
        MboyName.append("Amir")
        MboyName.append("Faizal")
        MboyName.append("Adam")
        MboyName.append("Ariff")
        MboyName.append("Zikri")
        MboyName.append("Umar")
        MboyName.append("Mohamed")
        MboyName.append("Imran")
        MboyName.append("Yusuf")
        MboyName.append("Suren J.Amarasekera")
        MboyName.append("Mahtab Uddin Ahmed")
        MboyName.append("Mohd Khairil Abdullah")
        MboyName.append("Tan Sri Jamaludin Ibrahim")
        MboyName.append("Mohd Asri Hassan")
        // Malay female name
        MgirlName.append("Nurul")
        MgirlName.append("Mira")
        MgirlName.append("Husna")
        MgirlName.append("Nabilah")
        MgirlName.append("Sophia")
        MgirlName.append("Fara")
        MgirlName.append("Nor")
        MgirlName.append("Siti")
        MgirlName.append("Puteri")
        MgirlName.append("Alya")
        MgirlName.append("Dian Siswarini")
        
        workerNameArr.append("tempName") // add in temp name in list
        for item in 1...maxWorkers {
            
            // set table
            let workerBG = SKSpriteNode(imageNamed: "slice1")
            workerBG.name = "slice_\(item)"
            workerBG.setScale(1)
            workerBG.position = CGPoint(x: firstX, y: firstY)
            workerBG.zPosition = 1
            self.addChild(workerBG)
            workerBG.isHidden = true
            workerBGSpriteArr.append(workerBG)
            
            // set table
            let workerTable = SKSpriteNode(imageNamed: "table")
            workerTable.setScale(1)
            workerTable.position = CGPoint(x: firstX, y: firstY)
            workerTable.zPosition = 2
            self.addChild(workerTable)
            
            // set gender (1 : guy, 2 : girl)
            let randomGender = randomInt(min: 1, max: 2)
            var gender = "guy"
            if(randomGender == 1) {
                gender = "guy"
            } else {
                gender = "girl"
            }
            
            // set worker face
            let randomFace = randomInt(min: 1, max: 3)
            let workerFace = SKSpriteNode(imageNamed: "face_normal_0\(randomFace)")
            workerFace.name = "workerFace_\(item)"
            workerFace.setScale(1)
            workerFace.position = CGPoint(x: firstX, y: firstY)
            workerFace.zPosition = 5
            self.addChild(workerFace)
            workerFaceSpriteArr.append(workerFace)
            
            if gender == "girl" && randomFace == 1 {
                let randomName = randomInt(min: 0, max: CgirlName.count - 1)
                workerNameArr.append(CgirlName[randomName])
            } else if gender == "girl" && randomFace == 2 {
                let randomName = randomInt(min: 0, max: MgirlName.count - 1)
                workerNameArr.append(MgirlName[randomName])
            } else if gender == "girl" && randomFace == 3 {
                let randomName = randomInt(min: 0, max: IgirlName.count - 1)
                workerNameArr.append(IgirlName[randomName])
            } else if gender == "guy" && randomFace == 1 {
                let randomName = randomInt(min: 0, max: CboyName.count - 1)
                workerNameArr.append(CboyName[randomName])
            } else if gender == "guy" && randomFace == 2 {
                let randomName = randomInt(min: 0, max: MboyName.count - 1)
                workerNameArr.append(MboyName[randomName])
            } else if gender == "guy" && randomFace == 3 {
                let randomName = randomInt(min: 0, max: IboyName.count - 1)
                workerNameArr.append(IboyName[randomName])
            }
            
            // set worker name
            let workerName = SKLabelNode(fontNamed:"H.H. Samuel")
            workerName.text = workerNameArr[item]
            workerName.fontSize = 36
            workerName.setScale(1)
            workerName.position = CGPoint(x: firstX, y: firstY - 180)
            workerName.zPosition = 11
            self.addChild(workerName)
            
            // set worker body
            let randomBody = randomInt(min: 1, max: 6)
            let workerBody = SKSpriteNode(imageNamed: "body_0\(randomBody)")
            workerBody.name = "worker_\(item)"
            workerBody.setScale(1)
            workerBody.position = CGPoint(x: firstX, y: firstY)
            workerBody.zPosition = 4
            self.addChild(workerBody)
            workerBodySpriteArr.append(workerBody)
            workerBodyColorArr.append(randomBody)
            
            // set worker body
            let workerDeadBody = SKSpriteNode(imageNamed: "hand_fainted_0\(randomBody)")
            workerDeadBody.name = "workerDead_\(item)"
            workerDeadBody.setScale(1)
            workerDeadBody.position = CGPoint(x: firstX, y: firstY)
            workerDeadBody.zPosition = 4
            workerDeadBody.isHidden = true
            addChild(workerDeadBody)
            workerDeadSpriteArr.append(workerDeadBody)
            
            // set worker hair
            var maxHairType = 0
            if gender == "guy" {
                maxHairType = 5
            } else if gender == "girl" {
                maxHairType = 4
            }
            let randomHairType = randomInt(min: 1, max: maxHairType)
            let randomHairColor = randomInt(min: 1, max: 6)
            
            let workerHair = SKSpriteNode(imageNamed: "\(gender)hair_0\(randomHairType)_0\(randomHairColor)")
            workerHair.name = "workerHair_\(item)"
            workerHair.setScale(1)
            workerHair.position = CGPoint(x: firstX, y: firstY)
            workerHair.zPosition = 6
            self.addChild(workerHair)
            
            // set face item (glasses)
            let randomGlasses = randomInt(min: 0, max: 3)
            
            let workerGlasses = SKSpriteNode(imageNamed: "glasses_0\(randomGlasses)")
            workerGlasses.name = "workerExpression_\(item)"
            workerGlasses.setScale(1)
            workerGlasses.position = CGPoint(x: firstX, y: firstY)
            workerGlasses.zPosition = 7
            
            if randomGlasses == 0 {
                workerGlasses.isHidden = true
            }
            else {
                workerGlasses.isHidden = false
            }
            self.addChild(workerGlasses)
            workerGlassesSpriteArr.append(workerGlasses)
            
            // set worker computer
            let workerComputer = SKSpriteNode(imageNamed: "table_01")
            workerComputer.name = "workerComputer_\(item)"
            workerComputer.setScale(1)
            workerComputer.position = CGPoint(x: firstX, y: firstY)
            workerComputer.zPosition = 9
            self.addChild(workerComputer)
            workerComputerSpriteArr.append(workerComputer)
            
            workerComputer.run(SKAction.repeatForever(SKAction.animate(with: textureArray_2, timePerFrame: 0.5)))
            
            // set filling white indicator bar
            let filled_bar = SKSpriteNode(imageNamed: "bar_00")
            filled_bar.name = "filled_bar_\(item)"
            filled_bar.setScale(1)
            filled_bar.anchorPoint = CGPoint(x: 0.0, y: 0.5)
            filled_bar.position = CGPoint(x: workerBody.position.x - (203 / 2), y: workerBody.position.y + 177)
            filled_bar.zPosition = 7
            addChild(filled_bar)
            workerFillingBarSpriteArr.append(filled_bar)
            
            // set worker job
            let randomNumber = randomInt(min: 1, max: 5)
            workerCurrentDocument.append(randomNumber)
            workerBoredAction.append(false)
            
            let jobs = SKSpriteNode(imageNamed: "document_small_\(randomNumber)")
            jobs.name = "document_\(item)"
            jobs.setScale(1)
            jobs.position = CGPoint(x: firstX + 60, y: firstY + 38)
            jobs.zPosition = 2
            self.addChild(jobs)
            workerDocumentSpriteArr.append(jobs)
            
            var randomNumber2 = 0
            
            if randomNumber <= 3 { // bored
                randomNumber2 = 1
            } else if randomNumber >= 4 && randomNumber <= 6 { // normal
                randomNumber2 = 2
            }
            else { // tension
                randomNumber2 = 3
            }
            
            // set indicator bar
            let bar = SKSpriteNode(imageNamed: "bar_0\(randomNumber2)")
            bar.name = "bar_\(item)"
            bar.setScale(1)
            bar.position = CGPoint(x: workerBody.position.x, y: workerBody.position.y + 177)
            bar.zPosition = 6
            addChild(bar)
            workerColorFillingBarSpriteArr.append(bar)
            
            // set worker document
            let documentCount = SKLabelNode(fontNamed:"AllerDisplay")
            documentCount.text = String(randomNumber)
            documentCount.name = "documentCount_\(item)"
            documentCount.fontSize = 36
            documentCount.setScale(1)
            documentCount.position = CGPoint(x: firstX + 60, y: firstY + 130)
            documentCount.zPosition = 11
            self.addChild(documentCount)
            
            // set face expression
            var tempFace = "face_01"
            var tempExpression = "face_01"
            if setWorkingSpeed(documentCount: randomNumber) == 2 {
                tempFace = "face_01"
                tempExpression = "expression_happy"
            } else if setWorkingSpeed(documentCount: randomNumber) == 3.3 {
                tempFace = "face_02"
                tempExpression = "expression_sweat"
            }
            else {
                tempFace = "face_03"
                tempExpression = "expression_cry"
            }
            
            // set face expression
            let workerExpression = SKSpriteNode(imageNamed: tempExpression)
            workerExpression.name = "workerExpression_\(item)"
            workerExpression.setScale(1)
            workerExpression.position = CGPoint(x: firstX, y: firstY)
            workerExpression.zPosition = 7
            addChild(workerExpression)
            workerExpressionSpriteArr.append(workerExpression)
            
            // set face expression indicator bar
            let expressionIndicator = SKSpriteNode(imageNamed: tempFace)
            expressionIndicator.name = "expressionIndicator_\(item)"
            expressionIndicator.setScale(1)
            expressionIndicator.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            expressionIndicator.position = CGPoint(x: workerBody.position.x, y: workerBody.position.y + 245)
            expressionIndicator.zPosition = 8
            addChild(expressionIndicator)
            workerExpressionIndicatorSpriteArr.append(expressionIndicator)
            
            // Add to array
            if workerNameArr[item] == "Tan Sri Jamaludin Ibrahim" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_01")
                workerFaceColorArr.append(1)
                workerHair.texture = SKTexture(imageNamed: "guyhair_04_03")
                workerGlasses.isHidden = false
                workerGlasses.texture = SKTexture(imageNamed: "glasses_01")
            }
            else if workerNameArr[item] == "Michael Kuehner" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_02")
                workerFaceColorArr.append(2)
                workerHair.texture = SKTexture(imageNamed: "guyhair_01_04")
                workerGlasses.isHidden = false
                workerGlasses.texture = SKTexture(imageNamed: "glasses_01")
            }
            else if workerNameArr[item] == "Dian Siswarini" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_01")
                workerFaceColorArr.append(1)
                workerHair.texture = SKTexture(imageNamed: "girlhair_01_01")
                workerGlasses.isHidden = false
                workerGlasses.texture = SKTexture(imageNamed: "glasses_03")
            }
            else if workerNameArr[item] == "Supun Weerasinghe" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_03")
                workerFaceColorArr.append(3)
                workerHair.texture = SKTexture(imageNamed: "guyhair_02_06")
                workerGlasses.isHidden = false
                workerGlasses.texture = SKTexture(imageNamed: "glasses_01")
            }
            else if workerNameArr[item] == "Thomas Hundt" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_02")
                workerFaceColorArr.append(2)
                workerHair.texture = SKTexture(imageNamed: "guyhair_01_06")
                workerGlasses.isHidden = false
                workerGlasses.texture = SKTexture(imageNamed: "glasses_01")
            }
            else if workerNameArr[item] == "Suren J.Amarasekera" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_02")
                workerFaceColorArr.append(2)
                workerHair.texture = SKTexture(imageNamed: "guyhair_04_04")
            }
            else if workerNameArr[item] == "Mahtab Uddin Ahmed" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_03")
                workerFaceColorArr.append(3)
                workerHair.texture = SKTexture(imageNamed: "guyhair_01_06")
            }
            else if workerNameArr[item] == "Mohd Khairil Abdullah" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_01")
                workerFaceColorArr.append(1)
                workerHair.texture = SKTexture(imageNamed: "guyhair_04_04")
            }
            else if workerNameArr[item] == "Mohd Asri Hassan" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_01")
                workerFaceColorArr.append(1)
                workerHair.texture = SKTexture(imageNamed: "guyhair_01_06")
            }
            else if workerNameArr[item] == "Suresh Sidhu" {
                workerFace.texture = SKTexture(imageNamed: "face_normal_03")
                workerFaceColorArr.append(3)
                workerHair.texture = SKTexture(imageNamed: "guyhair_05_01")
                workerGlasses.isHidden = false
                workerGlasses.texture = SKTexture(imageNamed: "glasses_01")
            }
            else {
                workerFaceColorArr.append(randomFace)
            }
            
            // add position
            firstX += 400
            percent += 1
            
            // jump line
            if percent == 4 || percent == 7 {
                firstY -= 530 // 380
                firstX = frame.midX - 400
            }
        }
    }
    
    func staticDocumentSetup() {
        let randomNumber = randomInt(min: minJob, max: maxJob)
        documentArr.append(randomNumber)
        
        for documents in 1...maxDocument {
            
            let randomNumber = randomInt(min: minJob, max: maxJob)
            documentArr.append(randomNumber)
            let staticDocument = SKSpriteNode(imageNamed: "document_\(randomNumber)")
            staticDocument.name = "staticDocument"
            staticDocument.setScale(1)
            staticDocument.position = CGPoint(x: CGFloat(800 + (130 * documents)), y: documentPositionY)
            staticDocument.zPosition = 4
            addChild(staticDocument)
            
            let staticDocumentCount = SKLabelNode(fontNamed:"AllerDisplay")
            staticDocumentCount.text = "\(randomNumber)"
            staticDocumentCount.fontSize = 50
            staticDocumentCount.fontColor = UIColor(colorLiteralRed: 76/255, green: 76/255, blue: 76/255, alpha: 1)
            staticDocumentCount.name = "staticDocumentCount"
            staticDocumentCount.position = CGPoint(x: CGFloat(800 + (130 * documents)), y: documentPositionY + 160)
            staticDocumentCount.zPosition = 10
            addChild(staticDocumentCount)
        }
    }
    
    func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    func changeWorkerDocumentSprite(node: SKSpriteNode) {
        
        var index = 0
        // workerCurrentDocument is array of all workers current document
        for item in workerCurrentDocument {
            
            // find workers document sprite
            let documentSprite: SKSpriteNode = childNode(withName: "document_\(index + 1)") as! SKSpriteNode
            let documentCountSprite: SKLabelNode = childNode(withName: "documentCount_\(index + 1)") as! SKLabelNode
            
            if documentSprite.name == node.name {
                // remove all action
                workerFillingBarSpriteArr[index].removeAllActions()
                documentSprite.removeAllActions()
                
                workerCurrentDocument[index] -= 1
                
                documentCountSprite.text = String(workerCurrentDocument[index])
                
                if item <= 0 {
                    workerCurrentDocument[index] = 0
                }
                
                if item <= 1 {
                    workerColorFillingBarSpriteArr[index].texture = SKTexture(imageNamed: "bar_00")
                    
                    if !workerBoredAction[index] {
                        workerBoredAction[index] = true
                    }
                } else if !documentSprite.isHidden {
                    // add filling action on the worker document bar
                    let startDuration = SKAction.wait(forDuration: TimeInterval(setWorkingSpeed(documentCount: workerCurrentDocument[index])))
                    let changeSprite = SKAction.run({ self.changeWorkerDocumentSprite(node: documentSprite)})
                    let runSequence = SKAction.sequence([startDuration, changeSprite])
                    let runForever = SKAction.repeatForever(runSequence)
                    workerDocumentSpriteArr[index].run(runForever, withKey: "ChangingSprite")
                    
                    // add filling action on the worker bar
                    let filledAction = SKAction.scaleX(to: 1, duration: TimeInterval(setWorkingSpeed(documentCount: workerCurrentDocument[index])))
                    let filledActionFull = SKAction.scaleX(to: 0, duration: 0)
                    let filledActionSequence = SKAction.sequence([filledActionFull, filledAction])
                    workerFillingBarSpriteArr[index].run(filledActionSequence)
                    
                    if setWorkingSpeed(documentCount: workerCurrentDocument[index]) == 2 {
                        workerExpressionIndicatorSpriteArr[index].texture = SKTexture(imageNamed: "face_01")
                        workerColorFillingBarSpriteArr[index].texture = SKTexture(imageNamed: "bar_01")
                        workerExpressionSpriteArr[index].texture = SKTexture(imageNamed: "expression_happy")
                        workerFaceSpriteArr[index].texture = SKTexture(imageNamed: "face_normal_0\(workerFaceColorArr[index])")
                        
                    } else if setWorkingSpeed(documentCount: workerCurrentDocument[index]) == 3.3 {
                        workerExpressionIndicatorSpriteArr[index].texture = SKTexture(imageNamed: "face_02")
                        workerColorFillingBarSpriteArr[index].texture = SKTexture(imageNamed: "bar_02")
                        workerExpressionSpriteArr[index].texture = SKTexture(imageNamed: "expression_sweat")
                        workerFaceSpriteArr[index].texture = SKTexture(imageNamed: "face_normal_0\(workerFaceColorArr[index])")
                        
                    } else {
                        workerExpressionIndicatorSpriteArr[index].texture = SKTexture(imageNamed: "face_03")
                        workerColorFillingBarSpriteArr[index].texture = SKTexture(imageNamed: "bar_03")
                        workerExpressionSpriteArr[index].texture = SKTexture(imageNamed: "expression_cry")
                        workerFaceSpriteArr[index].texture = SKTexture(imageNamed: "face_red_0\(workerFaceColorArr[index])")
                        
                    }
                    
                    workerExpressionIndicatorSpriteArr[index].isHidden = false
                    workerFillingBarSpriteArr[index].isHidden = false
                    workerColorFillingBarSpriteArr[index].isHidden = false
                }
                workerDocumentSpriteArr[index].texture = SKTexture(imageNamed: "document_small_\(workerCurrentDocument[index])")
            }
            index += 1
        }
    }
    
    func setWorkingSpeed(documentCount: Int) -> Float {
        
        var speed:Float = 0
        if documentCount <= 3 { // bored
            speed = 2.0
        } else if documentCount >= 4 && documentCount <= 6 { // normal
            speed = 3.3
        }
        else { // tension
            speed = 5
        }
        return speed
    }
    
    func resetDocument() {
        
        let previousDocument = workerCurrentDocument[currentDragNumber]
        workerCurrentDocument[currentDragNumber] += documentArr[0]
        
        var index = 0
        var needRemove = false
        var getIndex = [Int]()
        var checkBoring = 0
        var feelingBad = 0
        var isExhaustedSoundPlay = false
        var isSadSoundPlay = false
        // workerCurrentDocument is array of all workers current document
        for item in workerCurrentDocument {
            
            // find workers document sprite
            let yourSprite: SKSpriteNode = childNode(withName: "document_\(index + 1)") as! SKSpriteNode
            let documentCountSprite: SKLabelNode = childNode(withName: "documentCount_\(index + 1)") as! SKLabelNode
            
            yourSprite.texture = SKTexture(imageNamed: "document_\(item)")
            // reset the indicator bar if document is 0
            if previousDocument <= 0 && currentDragNumber == index{
                workerFaceSpriteArr[index].removeAllActions()
                workerBoredAction[index] = false
                // add filling action on the worker document bar
                let startDuration = SKAction.wait(forDuration: TimeInterval(setWorkingSpeed(documentCount: workerCurrentDocument[index])))
                let changeSprite = SKAction.run({ self.changeWorkerDocumentSprite(node: yourSprite)})
                let runSequence = SKAction.sequence([startDuration, changeSprite])
                let runForever = SKAction.repeatForever(runSequence)
                yourSprite.run(runForever, withKey: "ChangingSprite")
                
                // add filling action on the worker bar
                let filledAction = SKAction.scaleX(to: 1, duration: TimeInterval(setWorkingSpeed(documentCount: workerCurrentDocument[index])))
                let filledActionFull = SKAction.scaleX(to: 0, duration: 0)
                let filledActionSequence = SKAction.sequence([filledActionFull, filledAction])
                workerFillingBarSpriteArr[index].run(filledActionSequence)
                
                workerExpressionIndicatorSpriteArr[index].isHidden = false
                workerFillingBarSpriteArr[index].isHidden = false
                workerColorFillingBarSpriteArr[index].isHidden = false
            }
            
            if item == 0 {
                checkBoring += 1
                text_label.text = "\(workerNameArr[index+1]) feeling bored"
                text_label2.text = ""
                text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
            }
            
            if currentDragNumber == index {
                documentCountSprite.text = String(workerCurrentDocument[currentDragNumber])
                
                if setWorkingSpeed(documentCount: workerCurrentDocument[index]) == 2 {
                    workerExpressionIndicatorSpriteArr[index].texture = SKTexture(imageNamed: "face_01")
                    workerColorFillingBarSpriteArr[index].texture = SKTexture(imageNamed: "bar_01")
                    workerExpressionSpriteArr[index].texture = SKTexture(imageNamed: "expression_happy")
                    workerFaceSpriteArr[index].texture = SKTexture(imageNamed: "face_normal_0\(workerFaceColorArr[index])")
                    text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
                } else if setWorkingSpeed(documentCount: workerCurrentDocument[index]) == 3.3 {
                    workerExpressionIndicatorSpriteArr[index].texture = SKTexture(imageNamed: "face_02")
                    workerColorFillingBarSpriteArr[index].texture = SKTexture(imageNamed: "bar_02")
                    workerExpressionSpriteArr[index].texture = SKTexture(imageNamed: "expression_sweat")
                    workerFaceSpriteArr[index].texture = SKTexture(imageNamed: "face_normal_0\(workerFaceColorArr[index])")
                    text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
                } else {
                    workerExpressionIndicatorSpriteArr[index].texture = SKTexture(imageNamed: "face_03")
                    workerColorFillingBarSpriteArr[index].texture = SKTexture(imageNamed: "bar_03")
                    workerExpressionSpriteArr[index].texture = SKTexture(imageNamed: "expression_cry")
                    workerFaceSpriteArr[index].texture = SKTexture(imageNamed: "face_red_0\(workerFaceColorArr[index])")
                    feelingBad += 1
                    text_label.text = "\(workerNameArr[index+1]) feeling bad"
                    text_label2.text = ""
                    text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
                    isSadSoundPlay = true
                }
            }
            
            if item > 10 && !workerBodySpriteArr[index].isHidden{
                needRemove = true
                // temporary add index to an array
                getIndex.append(index + 1)
            }
            index += 1
        }
        
        if checkBoring == 0 && feelingBad == 0{
            text_label.text = "You are doing well"
            text_label2.text = ""
            text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
        }
        
        // too much works, worker too tired
        if needRemove {
            isExhaustedSoundPlay = true
            for item in getIndex
            {
                // find worker sprite
                let documentSprite: SKSpriteNode = childNode(withName: "document_\(item)") as! SKSpriteNode
                let hairSprite: SKSpriteNode = childNode(withName: "workerHair_\(item)") as! SKSpriteNode
                let bodySprite: SKSpriteNode = childNode(withName: "worker_\(item)") as! SKSpriteNode
                let documentCountSprite: SKLabelNode = childNode(withName: "documentCount_\(item)") as! SKLabelNode
                
                // set face position
                workerFaceSpriteArr[item - 1].position = CGPoint(x: workerFaceSpriteArr[item - 1].position.x + 40, y: workerFaceSpriteArr[item - 1].position.y - 52)
                hairSprite.position = CGPoint(x: hairSprite.position.x + 40, y: hairSprite.position.y - 52)
                workerExpressionSpriteArr[item - 1].position = CGPoint(x: workerExpressionSpriteArr[item - 1].position.x + 40, y: workerExpressionSpriteArr[item - 1].position.y - 52)
                
                workerFaceSpriteArr[item - 1].texture = SKTexture(imageNamed: "face_normal_0\(workerFaceColorArr[item - 1])")
                
                documentCountSprite.isHidden = true
                workerExpressionIndicatorSpriteArr[item - 1].isHidden = true
                
                workerComputerSpriteArr[item - 1].removeAllActions()
                workerComputerSpriteArr[item - 1].texture = SKTexture(imageNamed: "fainted_state_01")
                workerComputerSpriteArr[item - 1].run(SKAction.repeatForever(SKAction.animate(with: textureArray_3, timePerFrame: 0.5)))
                
                workerExpressionSpriteArr[item - 1].texture = SKTexture(imageNamed: "expression_fainted")
                documentSprite.isHidden = true
                bodySprite.isHidden = true
                workerDeadSpriteArr[item - 1].isHidden = false
                workerGlassesSpriteArr[item - 1].isHidden = true
                workerColorFillingBarSpriteArr[item - 1].isHidden = true
                workerFillingBarSpriteArr[item - 1].isHidden = true
                text_label.text = "\(workerNameArr[item]) is Exhausted"
                text_label2.text = ""
                text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
            }
            getIndex.removeAll()
        }
        
        if isExhaustedSoundPlay {
            run(sound_fainted)
        } else if isSadSoundPlay {
            run(sound_sad)
        } else if !isSadSoundPlay && !isExhaustedSoundPlay {
            run(sound_dropDocument)
        }
        // check end game
        var checkEnd = 0
        deadPeople = 0
        for item in 1...maxWorkers
        {
            let yourSprite2: SKSpriteNode = childNode(withName: "worker_\(item)") as! SKSpriteNode
            
            if yourSprite2.isHidden == true {
                checkEnd += 1
                deadPeople += 1
            }
        }
        
        // reset draggable document
        documentArr.remove(at: 0)
        
        draggableDocument.texture = SKTexture(imageNamed: "document_\(documentArr[0])")
        draggableDocument.name = "Draggable"
        draggableDocument.setScale(1)
        draggableDocument.position = CGPoint(x: frame.midX, y: documentPositionY)
        draggableDocument.zPosition = 21
        addChild(draggableDocument)
        
        self.enumerateChildNodes(withName: "staticDocument") {(bullet, stop) in
            bullet.removeFromParent()
        }
        
        self.enumerateChildNodes(withName: "staticDocumentCount") {(bullet, stop) in
            bullet.removeFromParent()
        }
        
        let staticDocumentCount = SKLabelNode(fontNamed:"AllerDisplay")
        staticDocumentCount.text = "\(documentArr[0])"
        staticDocumentCount.fontSize = 50
        staticDocumentCount.fontColor = UIColor.white
        staticDocumentCount.name = "staticDocumentCount"
        staticDocumentCount.position = CGPoint(x: frame.midX, y: documentPositionY + 160)
        staticDocumentCount.zPosition = 10
        addChild(staticDocumentCount)
        
        // add one more document to end
        let randomNumber = randomInt(min: minJob, max: maxJob)
        documentArr.append(randomNumber)
        
        for documents in 1...(documentArr.count - 1) {
            
            let staticDocument = SKSpriteNode(imageNamed: "document_\(documentArr[documents])")
            staticDocument.name = "staticDocument"
            staticDocument.setScale(1)
            staticDocument.position = CGPoint(x: CGFloat(800 + (130 * documents)), y: documentPositionY)
            staticDocument.zPosition = 4
            addChild(staticDocument)
            
            let staticDocumentCount = SKLabelNode(fontNamed:"AllerDisplay")
            staticDocumentCount.text = "\(documentArr[documents])"
            staticDocumentCount.fontSize = 50
            staticDocumentCount.fontColor = UIColor(colorLiteralRed: 76/255, green: 76/255, blue: 76/255, alpha: 1)
            staticDocumentCount.name = "staticDocumentCount"
            staticDocumentCount.position = CGPoint(x: CGFloat(800 + (130 * documents)), y: documentPositionY + 160)
            staticDocumentCount.zPosition = 10
            addChild(staticDocumentCount)
        }
        
        // remove draggable document action
        documentTimer.removeAllActions()
        
        let setNormal = SKAction.run(changeNormal)
        let setOrange = SKAction.run(changeOrange)
        let setRed = SKAction.run(changeRed)
        
        let filledActionFull = SKAction.scaleY(to: 0, duration: 0)
        let filledActionNormal = SKAction.scaleY(to: 0.4, duration: (TimeInterval(2 * documentTimerPercentage)))
        let filledActionMiddle = SKAction.scaleY(to: 0.8, duration: (TimeInterval(2 * documentTimerPercentage)))
        let filledActionHard = SKAction.scaleY(to: 1, duration: (TimeInterval(1 * documentTimerPercentage)))
        let filledActionBlock = SKAction.run(endGame)
        let filledActionSequence = SKAction.sequence([setNormal,filledActionFull, filledActionNormal, setOrange, filledActionMiddle, setRed, filledActionHard, filledActionBlock])
        let filledForever = SKAction.repeatForever(filledActionSequence)
        documentTimer.run(filledForever)
        
        currentDragNumber = -1
        
        if !bIsTriggerExhausted && needRemove {
            setUpTutorial(tutorialType: "TutorialExhausted")
        }
        
        // if all workers gone, game end
        if checkEnd == maxWorkers {
            endGame()
        }
    }
    
    func changeNormal() {
        documentTimer.texture = SKTexture(imageNamed: "timer")
    }
    
    func changeOrange() {
        documentTimer.texture = SKTexture(imageNamed: "timer_orange")
    }
    
    func changeRed() {
        documentTimer.texture = SKTexture(imageNamed: "timer_red")
        text_label.text = "Hurry Up"
        text_label2.text = ""
        text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
    }
    
    func endGame() {
        stopTimer();
        
        self.removeAllActions()
        self.removeAllChildren()
        
        // set background
        let bg = SKSpriteNode(imageNamed: "bg")
        bg.anchorPoint = CGPoint(x: 0.0, y: 1.0)
        bg.position = CGPoint(x: 0.0, y: frame.height)
        bg.size = CGSize(width: frame.width, height: frame.height)
        bg.zPosition = 0
        addChild(bg)
        
        let shapeSizeX: CGFloat = 1000
        let shapeSizeY: CGFloat = 1200
        let shape = SKShapeNode()
        shape.path = UIBezierPath(roundedRect: CGRect(x: -(shapeSizeX / 2), y: -(shapeSizeY / 2), width: shapeSizeX, height: shapeSizeY), cornerRadius: 30).cgPath
        shape.position = CGPoint(x: frame.midX, y: frame.midY + 100)
        shape.fillColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.strokeColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.lineWidth = 10
        shape.zPosition = 10
        addChild(shape)
        
        let shape2SizeX: CGFloat = 950
        let shape2SizeY: CGFloat = 1150
        let shape2 = SKShapeNode()
        shape2.path = UIBezierPath(roundedRect: CGRect(x: -(shape2SizeX / 2), y: -(shape2SizeY / 2), width: shape2SizeX , height: shape2SizeY), cornerRadius: 20).cgPath
        shape2.position = CGPoint(x: frame.midX, y: frame.midY + 100)
        shape2.fillColor = UIColor.white
        shape2.strokeColor = UIColor.white
        shape2.lineWidth = 10
        shape2.zPosition = 11
        addChild(shape2)
        
        let topLabel = SKLabelNode(fontNamed:"H.H. Samuel")
        topLabel.text = "Your colleagues are overworked"
        topLabel.fontColor = UIColor(colorLiteralRed: 75/255, green: 85/255, blue: 129/255, alpha: 1)
        topLabel.fontSize = 80
        topLabel.position = CGPoint(x: frame.midX, y: frame.midY + 450)
        topLabel.zPosition = 100
        addChild(topLabel)
        
        let endGameLabel1 = SKLabelNode(fontNamed:"AllerDisplay")
        endGameLabel1.text = "Highscore : \(highscore)"
        endGameLabel1.fontColor = UIColor(colorLiteralRed: 75/255, green: 85/255, blue: 129/255, alpha: 1)
        endGameLabel1.fontSize = 80
        endGameLabel1.position = CGPoint(x: frame.midX, y: frame.midY + 215)
        endGameLabel1.zPosition = 100
        addChild(endGameLabel1)
        
        let endGameLabel = SKLabelNode(fontNamed:"AllerDisplay")
        endGameLabel.text = "Current : \(currentScore)"
        endGameLabel.fontColor = UIColor(colorLiteralRed: 75/255, green: 85/255, blue: 129/255, alpha: 1)
        endGameLabel.fontSize = 80
        endGameLabel.position = CGPoint(x: frame.midX, y: frame.midY + 80)
        endGameLabel.zPosition = 100
        addChild(endGameLabel)
        
        let workerComputer = SKSpriteNode(imageNamed: "gameover_icon")
        workerComputer.setScale(1)
        workerComputer.position = CGPoint(x: frame.midX, y: frame.midY - 250)
        workerComputer.zPosition = 101
        addChild(workerComputer)
        
        let workerComputer1 = SKSpriteNode(imageNamed: "gameover_text")
        workerComputer1.setScale(1)
        workerComputer1.position = CGPoint(x: frame.midX, y: frame.midY + 700)
        workerComputer1.zPosition = 101
        addChild(workerComputer1)
        
        endGameButton.texture = SKTexture(imageNamed: "btn_continue_01")
        endGameButton.setScale(1)
        endGameButton.position = CGPoint(x: frame.midX, y: frame.midY - 680)
        endGameButton.name = "endGameButton"
        endGameButton.zPosition = 101
        addChild(endGameButton)
        
        let moveDown = SKAction.moveBy(x: 0, y: -50, duration: 0.2)
        shape.run(moveDown)
        shape2.run(moveDown)
        topLabel.run(moveDown)
        endGameLabel1.run(moveDown)
        endGameLabel.run(moveDown)
        workerComputer.run(moveDown)
        workerComputer1.run(moveDown)
        endGameButton.run(moveDown)
        
        bIsGameEnd = true
        
        sendHighscore()
    }
    
    // touch setup
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if bIsGameStart {
            if !bIsGameEnd {
                if bIsTriggerTutorial {
                    bIsTriggerTutorial = false
                    
                    blackBG.removeFromParent()
                    tutorial_label.removeFromParent()
                    tutorial_label.removeAllActions()
                    fingerShow.removeFromParent()
                    fingerShow.removeAllActions()
                    
                    if !bIsTriggerSwipe && triggerString == "TutorialSwipe"{
                        bIsTriggerSwipe = true
                        
                        // reset zPosition
                        // previous = 3
                        documentTimer.zPosition = 3
                        // previous = 10
                        staticDraggableDocumentCount.zPosition = 10
                        // previous = 21
                        draggableDocument.zPosition = 21
                        
                        setAction()
                    }
                    else if !bIsTriggerExhausted && triggerString == "TutorialExhausted"{
                        bIsTriggerExhausted = true
                        startTimer()
                        
                        // reset zPosition
                        // previous = 11
                        shape.zPosition = 11
                        // previous = 12
                        shape2.zPosition = 12
                        // previous = 20
                        text_label.zPosition = 20
                        text_label2.zPosition = 20
                        
                        documentTimer.speed = 1
                        for i in 0..<maxWorkers {
                            workerDocumentSpriteArr[i].speed = 1
                            workerFillingBarSpriteArr[i].speed = 1
                        }
                    }
                    else if !bIsTriggerSamePerson && triggerString == "TriggerSamePerson"{
                        bIsTriggerSamePerson = true
                        startTimer()
                        
                        // reset zPosition
                        // previous = 11
                        shape.zPosition = 11
                        // previous = 12
                        shape2.zPosition = 12
                        // previous = 20
                        text_label.zPosition = 20
                        text_label2.zPosition = 20
                        
                        documentTimer.speed = 1
                        for i in 0..<maxWorkers {
                            workerDocumentSpriteArr[i].speed = 1
                            workerFillingBarSpriteArr[i].speed = 1
                        }
                    }
                    triggerString = ""
                }
                else {
                    // Get the location of the touch in this scene
                    let touch = touches.first
                    let location = touch?.location(in: self)
                    // Check if the location of the touch is within the button's bounds
                    let touchNode = self.atPoint(location!)
                    
                    if let name = touchNode.name {
                        if name == draggableDocument.name {
                            draggableDocument.texture = SKTexture(imageNamed: "document_pull_\(documentArr[0])")
                        }
                    }
                }
            }
            else {
                for touch: AnyObject in touches {
                    // Get the location of the touch in this scene
                    let location = touch.location(in: self)
                    // Check if the location of the touch is within the button's bounds
                    if endGameButton.contains(location) {
                        endGameButton.texture = SKTexture(imageNamed: "btn_continue_02")
                    }
                }
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if bIsGameStart {
            if !bIsGameEnd {
                // where we touch
                let touch = touches.first
                let pointOfTouch = touch?.location(in: self)
                let previousPointOfTouch = touch?.previousLocation(in: self)
                
                if draggableDocument.frame.contains(previousPointOfTouch!) {
                    draggableDocument.position = pointOfTouch!
                    
                    for item in 1...maxWorkers {
                        
                        if workerBodySpriteArr[item - 1].name == "worker_\(item)" && workerBodySpriteArr[item - 1].frame.contains(draggableDocument.position) && !workerBodySpriteArr[item - 1].isHidden {
                            workerBGSpriteArr[item - 1].isHidden = false
                        }
                        else {
                            workerBGSpriteArr[item - 1].isHidden = true
                        }
                    }
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if bIsGameStart {
            if !bIsGameEnd {
                var bool = 0
                for item in 1...maxWorkers {
                    workerBGSpriteArr[item - 1].isHidden = true
                    
                    if workerBodySpriteArr[item - 1].name == "worker_\(item)" && workerBodySpriteArr[item - 1].frame.contains(draggableDocument.position) && workerBodySpriteArr[item - 1].isHidden == false && previousAssign != item {
                        currentDragNumber = item - 1
                        previousAssign = item
                        bool = 1
                    } else if workerBodySpriteArr[item - 1].name == "worker_\(item)" && workerBodySpriteArr[item - 1].frame.contains(draggableDocument.position) && workerBodySpriteArr[item - 1].isHidden == false && deadPeople == 5 {
                        currentDragNumber = item - 1
                        previousAssign = item
                        bool = 1
                    } else if workerBodySpriteArr[item - 1].name == "worker_\(item)" && workerBodySpriteArr[item - 1].frame.contains(draggableDocument.position) && workerBodySpriteArr[item - 1].isHidden == false && previousAssign == item {
                        text_label.text = "You can't assign work"
                        text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1790)
                        text_label2.text = "to a same person consecutively"
                        text_label2.position = CGPoint(x: frame.midX - (text_label2.frame.width / 2), y: 1715)
                        setUpTutorial(tutorialType: "TriggerSamePerson")
                    }
                }
                
                if bool == 1 {
                    bool = 0
                    draggableDocument.removeFromParent()
                    resetDocument()
                }
                else {
                    draggableDocument.texture = SKTexture(imageNamed: "document_\(documentArr[0])")
                    draggableDocument.position = CGPoint(x: frame.midX, y: documentPositionY)
                }
            } else{
                for touch: AnyObject in touches {
                    // Get the location of the touch in this scene
                    let location = touch.location(in: self)
                    // Check if the location of the touch is within the button's bounds
                    if endGameButton.contains(location) {
                        restartGame()
                    }
                }
            }
        }
    }
    
    func restartGame() {
        // do any ting after end game
        let scene = GameOne(size: self.size)
        scene.scaleMode = self.scaleMode
        let animation = SKTransition.crossFade(withDuration: 0.5)
        self.view?.presentScene(scene, transition: animation)
    }
    
    func getHighscore() {
        // get highscore from server
//        highscore = "get highscore api"
    }
    
    func sendHighscore() {
        // send highscore to server
        highscore = currentScore
    }
    
    func startNewGame() {
        
        self.removeAllActions()
        self.removeAllChildren()
        
        bIsGameStart = false
        
        getHighscore()
        
        // Load tutorial progression
        if highscore == 0 {
            bIsTriggerSwipe = false
            bIsTriggerExhausted = false
            bIsTriggerSamePerson = false
        } else {
            bIsTriggerSwipe = true
            bIsTriggerExhausted = true
            bIsTriggerSamePerson = true
        }
        
        // set background
        let bg = SKSpriteNode(imageNamed: "bg")
        bg.anchorPoint = CGPoint(x: 0.0, y: 1.0)
        bg.position = CGPoint(x: 0.0, y: frame.height)
        bg.size = CGSize(width: frame.width, height: frame.height)
        bg.zPosition = 0
        addChild(bg)
        
        draw_Rect()
        
        // set up text
        text_label.text = "3"
        text_label.fontSize = 70
        text_label.fontColor = UIColor(colorLiteralRed: 75/255, green: 85/255, blue: 129/255, alpha: 1)
        text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1750)
        text_label.zPosition = 20
        text_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        addChild(text_label)
        
        text_label2.text = ""
        text_label2.fontSize = 70
        text_label2.fontColor = UIColor(colorLiteralRed: 75/255, green: 85/255, blue: 129/255, alpha: 1)
        text_label2.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1750)
        text_label2.zPosition = 20
        text_label2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        addChild(text_label2)
        
        gameResetFromStart()
        
        let counterDecrement = SKAction.sequence([SKAction.wait(forDuration: 1.0),
                                                  SKAction.run(countdownAction)])
        let waitASec = SKAction.wait(forDuration: 1)
        let gameStart = SKAction.run {
            self.bIsGameStart = true
            self.blackBG.removeFromParent()
            self.mid_timer_label.removeFromParent()
            if !self.bIsTriggerSwipe {
                self.setUpTutorial(tutorialType: "TutorialSwipe")
            } else {
                self.setAction()
            }
        }
        run(SKAction.sequence([SKAction.repeat(counterDecrement, count: 3),
                               SKAction.run(endCountdown), waitASec, gameStart]))
        
        blackBG.size = CGSize(width: 1536, height: 2048)
        blackBG.color = UIColor(colorLiteralRed: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        blackBG.position = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height / 2)
        blackBG.zPosition = 50
        addChild(blackBG)
        
        mid_timer_label.fontSize = 160
        mid_timer_label.text = "3"
        mid_timer_label.color = UIColor.white
        mid_timer_label.position = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height / 2)
        mid_timer_label.zPosition = 51
        mid_timer_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        addChild(mid_timer_label)
    }
    
    func countdownAction() {
        timeCount -= 1
        text_label.text = "\(timeCount)"
        text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
        mid_timer_label.text = "\(timeCount)"
    }
    
    func endCountdown() {
        text_label.text = "START"
        text_label.position = CGPoint(x: frame.midX - (text_label.frame.width / 2), y: 1755)
        mid_timer_label.text = "START"
    }
    
    func gameResetFromStart() {
        
        let loopAction = SKAction.repeatForever(sound_key01)
        run(loopAction)
        
        documentTimer.removeAllActions()
        // timer
        second = 0
        timer = Timer()
        isTimerRunning = false
        timeCount = 3
        documentTimerPercentage = 1
        secondCalculatePercentage = 0
        
        // Text
        // document
        currentDragNumber = -1
        
        documentArr = [Int]()
        currentDocumentArr = [Int]()
        workerCurrentDocument = [Int]()
        workerBoredAction = [Bool]()
        workerBodyColorArr = [Int]()
        workerFaceColorArr = [Int]()
        
        // set all sprite into array
        workerBodySpriteArr = [SKSpriteNode]()
        workerFaceSpriteArr = [SKSpriteNode]()
        workerGlassesSpriteArr = [SKSpriteNode]()
        workerExpressionIndicatorSpriteArr = [SKSpriteNode]()
        workerExpressionSpriteArr = [SKSpriteNode]()
        workerComputerSpriteArr = [SKSpriteNode]()
        workerBGSpriteArr = [SKSpriteNode]()
        workerDocumentSpriteArr = [SKSpriteNode]()
        workerDeadSpriteArr = [SKSpriteNode]()
        workerFillingBarSpriteArr = [SKSpriteNode]()
        workerColorFillingBarSpriteArr = [SKSpriteNode]()
        
        // score
        currentScore = 0
        bIsGameEnd = false
        deadPeople = 0
        
        // init varible
        workerNameArr = [String]()
        CboyName = [String]()
        CgirlName = [String]()
        MboyName = [String]()
        MgirlName = [String]()
        IboyName = [String]()
        IgirlName = [String]()
        
        textureArray_1 = [SKTexture]()
        textureArray_2 = [SKTexture]()
        textureArray_3 = [SKTexture]()
        
        previousAssign = -2
        
        textureArray_1.append(SKTexture(imageNamed: "boss_01"))
        textureArray_1.append(SKTexture(imageNamed: "boss_02"))
        
        textureArray_2.append(SKTexture(imageNamed: "table_01"))
        textureArray_2.append(SKTexture(imageNamed: "table_02"))
        
        textureArray_3.append(SKTexture(imageNamed: "fainted_state_01"))
        textureArray_3.append(SKTexture(imageNamed: "fainted_state_02"))
        
        // set up workers
        workerSetup()
        
        // set up static documents
        staticDocumentSetup()
        
        // set boss
        let boss = SKSpriteNode(imageNamed: "boss_01")
        boss.setScale(1)
        boss.position = CGPoint(x: 428, y: 1365)
        boss.zPosition = 10
        addChild(boss)
        boss.run(SKAction.repeatForever(SKAction.animate(with: textureArray_1, timePerFrame: 0.5)))
        
        // set up clock
        timer_label.text = "00:00"
        timer_label.fontSize = 80
        timer_label.fontColor = UIColor(colorLiteralRed: 88/255, green: 136/255, blue: 184/255, alpha: 1)
        timer_label.position = CGPoint(x: frame.midX - 20, y: 1950)
        timer_label.zPosition = 10
        timer_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        addChild(timer_label)
        
        let clock = SKSpriteNode(imageNamed: "icon_clock")
        clock.setScale(1)
        clock.position = CGPoint(x: frame.midX - 150, y: 1975)
        clock.zPosition = 10
        addChild(clock)
        
        // set up draggable document
        draggableDocument_BG.setScale(1)
        draggableDocument_BG.position = CGPoint(x: frame.midX, y: documentFramePositionY)
        draggableDocument_BG.zPosition = 2
        addChild(draggableDocument_BG)
        
        documentTimer.setScale(1)
        documentTimer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        documentTimer.position = CGPoint(x: frame.midX, y: 1268)
        documentTimer.zPosition = 3
        addChild(documentTimer)
        
        draggableDocument.texture = SKTexture(imageNamed: "document_\(documentArr[0])")
        draggableDocument.name = "Draggable"
        draggableDocument.setScale(1)
        draggableDocument.position = CGPoint(x: frame.midX, y: documentPositionY)
        draggableDocument.zPosition = 21
        addChild(draggableDocument)
        
        staticDraggableDocumentCount.text = "\(documentArr[0])"
        staticDraggableDocumentCount.fontSize = 50
        staticDraggableDocumentCount.fontColor = UIColor.white
        staticDraggableDocumentCount.name = "staticDocumentCount"
        staticDraggableDocumentCount.position = CGPoint(x: frame.midX, y: documentPositionY + 160)
        staticDraggableDocumentCount.zPosition = 10
        addChild(staticDraggableDocumentCount)
    }
    
    // set action on start game
    func setAction() {
        
        text_label.text = ""
        text_label2.text = ""
        
        // set action on draggable document
        let setNormal = SKAction.run(changeNormal)
        let setOrange = SKAction.run(changeOrange)
        let setRed = SKAction.run(changeRed)
        
        let filledActionFull = SKAction.scaleY(to: 0, duration: 0)
        let filledActionNormal = SKAction.scaleY(to: 0.4, duration: (TimeInterval(2 * documentTimerPercentage)))
        let filledActionMiddle = SKAction.scaleY(to: 0.8, duration: (TimeInterval(2 * documentTimerPercentage)))
        let filledActionHard = SKAction.scaleY(to: 1, duration: (TimeInterval(1 * documentTimerPercentage)))
        let filledActionBlock = SKAction.run(endGame)
        let filledActionSequence = SKAction.sequence([setNormal,filledActionFull, filledActionNormal, setOrange, filledActionMiddle, setRed, filledActionHard, filledActionBlock])
        let filledForever = SKAction.repeatForever(filledActionSequence)
        documentTimer.run(filledForever)
        
        for item in 1...maxWorkers {
            // add filling action on the worker bar
            let filledAction = SKAction.scaleX(to: 1, duration: TimeInterval(setWorkingSpeed(documentCount: workerCurrentDocument[item - 1])))
            let filledActionFull = SKAction.scaleX(to: 0, duration: 0)
            let filledActionSequence = SKAction.sequence([filledActionFull, filledAction])
            workerFillingBarSpriteArr[item - 1].run(filledActionSequence)
            
            // add filling action on the worker document bar
            let startDuration = SKAction.wait(forDuration: TimeInterval(setWorkingSpeed(documentCount: workerCurrentDocument[item - 1])))
            let changeSprite = SKAction.run({ self.changeWorkerDocumentSprite(node: self.workerDocumentSpriteArr[item - 1])})
            let runSequence = SKAction.sequence([startDuration, changeSprite])
            let runForever = SKAction.repeatForever(runSequence)
            workerDocumentSpriteArr[item - 1].run(runForever, withKey: "ChangingSprite")
        }
        // start timer
        startTimer()
    }
    
    func setUpTutorial(tutorialType: String) {
        var triggerTutorial = false
        
        if tutorialType == "TutorialSwipe" && !bIsTriggerSwipe{
            triggerTutorial = true
            triggerString = "TutorialSwipe"
            
            // previous = 2
            //            draggableDocument_BG.zPosition = 49
            // previous = 3
            documentTimer.zPosition = 52
            // previous = 10
            staticDraggableDocumentCount.zPosition = 53
            // previous = 21
            draggableDocument.zPosition = 54
            
            // add finger action
            let actionSetPosition = SKAction.run {
                self.fingerShow.position = CGPoint(x: self.frame.midX + 100, y: self.documentPositionY - 150)
            }
            let actionDown = SKAction.run {
                self.fingerShow.texture = SKTexture(imageNamed: "finger_down")
            }
            let actionWait = SKAction.wait(forDuration: 0.2)
            let actionMove = SKAction.moveTo(y: documentPositionY - 200 - 500, duration: 1)
            let actionUp = SKAction.run {
                self.fingerShow.texture = SKTexture(imageNamed: "finger_up")
            }
            let actionSequence = SKAction.sequence([actionSetPosition, actionWait, actionDown, actionWait, actionMove, actionUp, actionWait])
            let runForever = SKAction.repeatForever(actionSequence)
            fingerShow.run(runForever)
        }
        else if tutorialType == "TutorialExhausted" && !bIsTriggerExhausted {
            triggerTutorial = true
            stopTimer()
            triggerString = "TutorialExhausted"
            
            // previous = 11
            shape.zPosition = 51
            // previous = 12
            shape2.zPosition = 52
            // previous = 20
            text_label.zPosition = 53
            text_label2.zPosition = 53
            
            // no need finger
            fingerShow.isHidden = true
            documentTimer.speed = 0
            
            for i in 0..<maxWorkers {
                workerDocumentSpriteArr[i].speed = 0
                workerFillingBarSpriteArr[i].speed = 0
            }
        }
        else if tutorialType == "TriggerSamePerson" && !bIsTriggerSamePerson {
            
            triggerTutorial = true
            stopTimer()
            triggerString = "TriggerSamePerson"
            
            // previous = 11
            shape.zPosition = 51
            // previous = 12
            shape2.zPosition = 52
            // previous = 20
            text_label.zPosition = 53
            text_label2.zPosition = 53
            
            // no need finger
            fingerShow.isHidden = true
            documentTimer.speed = 0
            
            for i in 0..<maxWorkers {
                workerDocumentSpriteArr[i].speed = 0
                workerFillingBarSpriteArr[i].speed = 0
            }
        }
        
        if triggerTutorial {
            bIsTriggerTutorial = true
            
            // tutorial assets
            blackBG.size = CGSize(width: 1536, height: 2048)
            blackBG.color = UIColor(colorLiteralRed: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
            blackBG.position = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height / 2)
            blackBG.zPosition = 50
            addChild(blackBG)
            
            tutorial_label.text = "Press Any To Continue..."
            tutorial_label.fontSize = 55
            tutorial_label.fontColor = UIColor.white
            tutorial_label.position = CGPoint(x: frame.midX, y: 300)
            tutorial_label.zPosition = 51
            tutorial_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            addChild(tutorial_label)
            
            let actionFadeIn = SKAction.fadeAlpha(to: 0.5, duration: 0.5)
            let actionFadeOut = SKAction.fadeAlpha(to: 1, duration: 0.5)
            let actionSequence = SKAction.sequence([actionFadeIn, actionFadeOut])
            let actionLoop = SKAction.repeatForever(actionSequence)
            tutorial_label.run(actionLoop)
            
            fingerShow.setScale(1)
            fingerShow.zPosition = 55
            addChild(fingerShow)
        }
    }
}
